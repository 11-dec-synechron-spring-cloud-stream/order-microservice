package com.classpath.ordermicroservice.processor;

import com.classpath.ordermicroservice.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
@Slf4j
public class OrderConsumer {
    @Bean
    public Consumer<Order> processOrder() {
        return (order) -> {
                   log.info("Inside the processing method of Order processor");
                   log.info(" Order :: {}", order);
        };
    }
}