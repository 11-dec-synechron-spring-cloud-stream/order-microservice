package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import java.time.LocalDate;
import java.util.function.Function;
import java.util.function.Supplier;

@Configuration
@Slf4j
public class OrderService {

    public Order saveOrder(Order order) {
      log.info("Saving the order in the DB");
      order.setOrderId(Math.round(Math.random()* 3333333333L));
      return order;
    }

    @Bean
    public Supplier<Message<Order>> publishOrder (){
        return  () -> MessageBuilder
                        .withPayload(
                                Order.builder()
                                            .orderId(1111)
                                            .date(LocalDate.now())
                                            .name("suresh")
                                            .price(22000)
                                            .build())
                                .build();
    }

    @Bean
    public Supplier<Message<Order>> publishOrder2 (){
        return  () -> MessageBuilder
                        .withPayload(
                                Order.builder()
                                            .orderId(1111)
                                            .date(LocalDate.now())
                                            .name("suresh")
                                            .price(22000)
                                            .build())
                                .build();
    }

    @Bean
    public Function<Order, String> mapOrderToCustomerName() {
        return order -> {
            System.out.println("Received: " + order);
            return order.getName();
        };
    }
}