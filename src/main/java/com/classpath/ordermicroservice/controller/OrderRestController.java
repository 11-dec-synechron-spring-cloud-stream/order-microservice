package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.messaging.Message;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
public class OrderRestController {

    private final OrderService orderService;

    private final StreamBridge streamBridge;

    @PostMapping
    public Order publishOrder(@RequestBody Order order){

        // final Message<Order> orderMessage = this.orderService.publishOrder().get();
        Order savedOrder = this.orderService.saveOrder(order);
        //fire an event
        this.streamBridge.send("order-out-0", savedOrder);
        return savedOrder;
    }
}